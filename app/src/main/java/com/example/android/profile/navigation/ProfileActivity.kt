package com.example.android.profile.navigation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import androidx.navigation.Navigation

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)


    }

    override fun onSupportNavigateUp() =
            Navigation.findNavController(this, R.id.my_profile_host_fragment).navigateUp()

}
